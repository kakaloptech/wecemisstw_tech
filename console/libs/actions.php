<?php

	class Actions {

		function __construct() {
			$this->campaignName = 'wecemiss';
		}
		
		function post_data($url = '', $data = array()) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:3000/".$this->campaignName.$url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); 
			$result = curl_exec($ch); 
			curl_close($ch);

			return $result;
		}

		function get_data($url = '', $data = array()) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:3000/".$this->campaignName.$url."?".http_build_query($data));
			$result = curl_exec($ch); 
			curl_close($ch);

			return $result;
		}

		function generate_seach($temp = array()) {
			if(!empty($temp['q'])) {
				$data['q'] = $temp['q'];
			}

			$data['sDate'] = ((empty($temp['sDate'])) ? date('Y-m-d', strtotime('-30 days')) : $temp['sDate']).' 00:00:00';
			$data['eDate'] = ((empty($temp['eDate'])) ? date('Y-m-d') : $temp['eDate']).' 23:59:59';
			$data['page'] = $temp['page'];

			return $data;
		}
		
	}

?>