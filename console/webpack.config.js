var path = require('path');
var webpack = require("webpack");
var commonsPlugin = new webpack.optimize.CommonsChunkPlugin('common');
var uglifyJsPlugin = new webpack.optimize.UglifyJsPlugin({
  comments: false
});

module.exports = {
	entry: {
    login: './srcs/login.js',
		registe: './srcs/registe.js',
		transfer: './srcs/transfer.js',
		registe_d: './srcs/registe_d.js'
  },
	output: {
		path: path.resolve(__dirname, './comps'),
		publicPath: '/comps/',
		filename: '[name].min.js'
	},
  plugins: [commonsPlugin, uglifyJsPlugin],
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					plugins: ['transform-runtime'],
					presets: ['react', 'es2015', 'stage-0']
				}
			}
		]
	}
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    })
  ])
}

module.exports.plugins = (module.exports.plugins || []).concat([
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      warnings: false
    }
  })
])