<?php 
	include('./init/f_init.php');
	$Tokens->check_login_status();
	$Tokens->set_token();
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo CAMNAME; ?> 報名頁面</title>
	<?php include('header.php'); ?>
</head>
<body>
	<div class="container" id="MainBox"></div>
	<script type="text/javascript" defer>
    var _token = "<?php echo $Tokens->get_token(); ?>";
    var camName = "<?php echo CAMNAME; ?>";
  </script>
  <script type="text/javascript" defer src="js/allfns.js"></script>
  <script type="text/javascript" defer src="//localhost:8080/comps/common.min.js"></script>
  <script type="text/javascript" defer src="//localhost:8080/comps/registe_d.min.js"></script>
</body>
</html>