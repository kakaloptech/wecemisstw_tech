import React from 'react';
import ReactDOM from 'react-dom';

import { TopMenu } from './topmenu';
import TransferList from './comps/transfer_list';

const changePage = (page) => {
	$(window).scrollTop(0, 0);
	if(page !== window.location.pathname) {
		window.history.pushState(null, null, page);
	}
	execRender();
}

const Transfer = () => {
	return (
		<div>
			<TopMenu title={camName} />
			<div className="MainScope">
				<ol className="breadcrumb">
				  <li className="active">匯款列表</li>
				</ol>
				<TransferList changePage={changePage} />
			</div>
		</div>
	)
}

const execRender = () => {
	ReactDOM.render(
		<Transfer />,
		document.getElementById('MainBox')
	);
}

execRender();

$('.TopMenu li').eq(1).addClass('active');