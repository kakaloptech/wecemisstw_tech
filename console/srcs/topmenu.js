import React from 'react';

const TopMenu = (props) => {
  return (
    <div className="TopMenu">
      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="navbar-header">
          <a className="navbar-brand" 
              href="registe.php" >
            {props.title}
          </a>
        </div>
      </nav>

      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="navbar-header">
          <button type="button" 
                  className="navbar-toggle collapsed" 
                  data-toggle="collapse" 
                  data-target="#navbar" 
                  aria-expanded="false" 
                  aria-controls="navbar">
            <span className="sr-only">選單</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <a className="navbar-brand" 
              href="registe.php" >
            {props.title}
          </a>
        </div>
        <div id="navbar" className="navbar-collapse collapse">
          <ul className="nav navbar-nav TopMenu">
            <li>
              <a href="registe.php" >
                報名列表
              </a>
            </li>
            <li>
              <a href="transfer.php">
                匯款列表
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  )
}

module.exports = {
  TopMenu
}