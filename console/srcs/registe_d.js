import React from 'react';
import ReactDOM from 'react-dom';

import { TopMenu } from './topmenu';
import RegisteInfo from './comps/registe_info';

const RegisteD = () => {
	return (
		<div>
			<TopMenu title={camName} />
			<div className="MainScope">
				<ol className="breadcrumb">
				  <li><a href="registe.php">報名列表</a></li>
				  <li className="active">參賽者資料</li>
				</ol>
				<RegisteInfo />
			</div>
		</div>
	)
}

ReactDOM.render(
	<RegisteD />,
	document.getElementById('MainBox')
);

$('.TopMenu li').eq(0).addClass('active');