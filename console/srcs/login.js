import React from 'react';
import ReactDOM from 'react-dom';

class LoginForm extends React.Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	camName
	  };
	}
  _checklogin() {
		let account = document.getElementById('InputAccount').value;
		let passwd = document.getElementById('InputPassword').value;

		if (account === '' || account === undefined) {
			alert('請輸入登入的帳號');
			return;
		} else if (passwd === '' || passwd === undefined) {
			alert('請輸入登入的密碼');
			return;
		} else {
			let data = {
				type: 'login',
				account: account,
				passwd: passwd
			};
			let loginResult = (result) => {
				if (result['status']) {
					alert('登入成功');
					window.location = 'list.php';
				} else {
					alert('登入失敗，' + result['message']);
				}
				return;
			}

			post_data('procs.php', data, loginResult);
		}
	}
  render() {
    return (
      <div className="bs-example col-md-6 col-md-offset-3">
				<h3>{camName} 登入</h3>
				<hr />
				<div className="form-group">
					<label htmlFor="InputAccount">登入帳號</label>
					<input className="form-control" id="InputAccount" placeholder="登入帳號" type="text" />
				</div>
				<div className="form-group">
					<label htmlFor="InputPassword">登入密碼</label>
					<input className="form-control" id="InputPassword" placeholder="登入密碼" type="password" />
				</div>
				<button className="btn btn-primary" onClick={this._checklogin}>登入</button>
				<button className="btn btn-default LoginBtnLeft">離開</button>
			</div>
    )
  }
}

ReactDOM.render(
	<LoginForm />,
	document.getElementById('MainBox')
);