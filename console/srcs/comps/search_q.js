import React from 'react';
import Moment from 'moment';

export default class SearchQ extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      isShow: false,
      triangleIcon: 'glyphicon-triangle-top'
    };
  }
  componentDidMount() {
    document.getElementById('SearchQScope').style.display = "none";
  }
  _callSearch() {
    this.setState({
      isShow: !this.state.isShow,
      triangleIcon: (!this.state.isShow) ? 'glyphicon-triangle-bottom' : 'glyphicon-triangle-top'
    }, () => {
      document.getElementById('SearchQScope').style.display = (this.state.isShow) ? 'block' : 'none';
    });
  }
  _goQPage() {
    let url = window.location.pathname,
        q = $('#searchQ').val();

    window.history.pushState(null, null, url + '?q=' + q);

    this._callSearch();

    $('#searchQ').val('');

    this.props.setSearchQ(q, '1');
  }
  render() {
    return (
      <div className="">
        <a href="javascript:;" onClick={this._callSearch.bind(this)}>
          搜尋關鍵字
          <span className={['glyphicon', this.state.triangleIcon].join(' ')}></span>
        </a>
        <hr />
        <div id="SearchQScope">
          <div className="form-inline">
            <div className="form-group SearchSDateLabel">
              <label htmlFor="searchSDate">關鍵字</label>
            </div>
            <div className="form-group input-group-sm">
              <input type="text" className="form-control" id="searchQ" />
            </div>
            <div className="form-group SearchBtn">
              <button type="button" className="btn btn-primary btn-sm" onClick={this._goQPage.bind(this)}>搜尋</button>
            </div>
          </div>
          <hr />
        </div>
      </div>
    );
  }
}