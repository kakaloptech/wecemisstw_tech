import React from 'react';

const Pagination = (props) => {
	let totalPage = props.totalPage || 1,
			i = 1,
			pageArr = [];

	while(i <= totalPage) {
		pageArr.push(i);
		i += 1;
	}

	const go_to_page = (event) => {
		let url = window.location.pathname,
				params = get_url_vars(),
				page = event.target.value,
				newUrl = url + '?';
		if(params['sDate']) {
			newUrl += ('sDate=' + params['sDate'] + '&');
		}
		if(params['eDate']) {
			newUrl += ('eDate=' + params['eDate'] + '&');
		}
		if(params['q']) {
			newUrl += ('q=' + params['q'] + '&');
		}
		newUrl += 'page=' + page;
		window.history.pushState(null, null, newUrl);
		props.setPage(page);
	}

	return (
		<div>
			<div className="col-md-1" style={{fontSize: 13, padding: 0}}>
				目前頁數：
			</div>
			<div className="col-md-1" style={{padding: 0}}>
		 		<select className="form-control input-sm"
								onChange={go_to_page}
								value={props.page}>
		  		{pageArr.map((value, index) => {
		  			return (
		  				<option key={index}
		  								value={value}>
								{value}
							</option>
						)
		  		})}
		  	</select>
			</div>
		</div>
	)
}

module.exports = Pagination;

// <li>
// 	<div className="col-md-2" style={{ marginLeft: '32%' }}>
// 		<select className="form-control input-sm"
// 						onChange={go_to_page}
// 						value={props.page}>
//   		{pageArr.map((value, index) => {
//   			return (
//   				<option key={index}
//   								value={value}>
// 						{value}
// 					</option>
// 				)
//   		})}
//   	</select>
// 	</div>
// </li>
// 
// <div className="col-md-3 col-md-offset-7">
// 	<nav>
// 	  <ul className="pager">
// 	    <li className="disabled"><a href="#"><span aria-hidden="true">&larr;</span> 上一頁</a></li>
// 	    <li className="disabled"><a href="#">下一頁 <span aria-hidden="true">&rarr;</span></a></li>
// 	  </ul>
// 	</nav>
// </div>