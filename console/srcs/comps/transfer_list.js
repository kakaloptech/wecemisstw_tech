import React from 'react';

import Pagination from './pagination';
import SearchDate from './search_date';
import SearchQ from './search_q';

const ListCard = (props) => {
	return (
		<tr>
			<td>{props.data.name}</td>
			<td>{props.data.phone}</td>
			<td>{props.data.account_name}</td>
			<td>{props.data.account}</td>
			<td>
				<a 
					href="javascript:;"
					style={{textDecoration: 'none'}} >
					確認匯款
				</a>
			</td>
		</tr>
	)
}


export default class TransferList extends React.Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	page: get_url_vars()['page'] || 1,
	  	sDate: '',
	  	eDate: '',
	  	q: '',
	  	totalPage: 1,
	  	transfers: []
	  };
	}

	componentDidMount() {
		let params = get_url_vars(),
				sDate = params['sDate'] || '',
				eDate = params['eDate'] || '',
				q = params['q'] || '',
				page = params['page'] || 1;
		this.setState({
			sDate,
			eDate,
			q,
			page
		}, () => {
			this.get_transfers();
		});
	}

	get_transfers() {
		let page = this.state.page,
				sDate = this.state.sDate,
				eDate = this.state.eDate,
				q = this.state.q,
				transfers = [];

		const cbResult = (result) => {
			this.setState({
				totalPage: result['totals'],
	  		transfers: result['lists']
			});
		}

		post_data('procs.php', {
			type: 'getTransfer',
			sDate,
			eDate,
			q,
			page
		}, cbResult);

		this.setState({
			transfers
		});
	}

	set_page(page) {
		this.setState({
			page
		}, () => {
			this.get_transfers();
		});
	}

	set_search_date(sDate, eDate, page) {
		let q = '';
		this.setState({
			sDate,
			eDate,
			q,
			page
		}, () => {
			this.get_transfers();
		});
	}

	set_search_q(q, page) {
		let sDate = '',
				eDate = ''
		this.setState({
			sDate,
			eDate,
			q,
			page
		}, () => {
			this.get_transfers();
		});
	}

	render() {
		return (
			<div>
				<SearchDate setSearchDate={this.set_search_date.bind(this)} />
				<SearchQ setSearchQ={this.set_search_q.bind(this)} />
				<table className="table table-bordered">
				  <thead>
				  	<tr>
				  		<th className="col-md-2">姓名</th>
				  		<th className="col-md-3">電話</th>
				  		<th className="col-md-2">匯款戶名</th>
				  		<th className="col-md-4">匯款帳號</th>
				  		<th className="col-md-1">#</th>
				  	</tr>
				  </thead>
				  <tbody>
				  	{this.state.transfers.map((value, index) => {
				  		return (
				  			<ListCard data={value}
				  								key={index} />
			  			)
				  	})}
				  </tbody>
				</table>
				<Pagination 
					totalPage={this.state.totalPage}
					page={this.state.page} 
					setPage={this.set_page.bind(this)} />
			</div>
		)
	}
}