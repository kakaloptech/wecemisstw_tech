import React from 'react';

import Pagination from './pagination';
import SearchDate from './search_date';
import SearchQ from './search_q';

const ListCard = (props) => {
	return (
		<tr>
			<td>
				<a 
					href={"registe_d.php?id=" + props.data.id}
					style={{textDecoration: 'none'}} >
					{props.data.name}
				</a>
			</td>
			<td>{props.data.phone}</td>
			<td>{props.data.email}</td>
			<td>{props.data.guardian_name}</td>
			<td>{props.data.guardian_phone}</td>
			<td>
				<a 
					href="javascript:;"
					style={{textDecoration: 'none'}} >
					上架
				</a>
			</td>
		</tr>
	)
}

export default class RegisteList extends React.Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	page: 1,
	  	sDate: '',
	  	eDate: '',
	  	q: '',
	  	totalPage: 1,
	  	registes: []
	  };
	}

	componentDidMount() {
		let params = get_url_vars(),
				sDate = params['sDate'] || '',
				eDate = params['eDate'] || '',
				q = params['q'] || '',
				page = params['page'] || 1;
		this.setState({
			sDate,
			eDate,
			q,
			page
		}, () => {
			this.get_registes();
		});
	}

	get_registes() {
		let page = parseInt(this.state.page),
				sDate = this.state.sDate,
				eDate = this.state.eDate,
				q = this.state.q,
				registes = [];

		const cbResult = (result) => {
			this.setState({
				totalPage: result['totals'],
	  		registes: result['lists']
			});
		}

		post_data('procs.php', {
			type: 'getRegiste',
			sDate,
			eDate,
			q,
			page
		}, cbResult);

		this.setState({
			registes
		});
	}

	set_page(page) {
		this.setState({
			page
		}, () => {
			this.get_registes();
		});
	}

	set_search_date(sDate, eDate, page) {
		let q = '';
		this.setState({
			sDate,
			eDate,
			q,
			page
		}, () => {
			this.get_registes();
		});
	}

	set_search_q(q, page) {
		let sDate = '',
				eDate = ''
		this.setState({
			sDate,
			eDate,
			q,
			page
		}, () => {
			this.get_registes();
		});
	}

	render() {
		return (
			<div>
				<SearchDate setSearchDate={this.set_search_date.bind(this)} />
				<SearchQ setSearchQ={this.set_search_q.bind(this)} />
				<table className="table table-bordered">
				  <thead>
				  	<tr>
				  		<th className="col-md-2">姓名</th>
				  		<th className="col-md-2">電話</th>
				  		<th className="col-md-2">信箱</th>
				  		<th className="col-md-2">監護人</th>
				  		<th className="col-md-2">監護人電話</th>
				  		<th className="col-md-2">#</th>
				  	</tr>
				  </thead>
				  <tbody>
				  	{this.state.registes.map((value, index) => {
				  		return (
				  			<ListCard data={value}
				  								key={index} />
			  			)
				  	})}
				  </tbody>
				</table>
				<Pagination 
					totalPage={this.state.totalPage}
					page={this.state.page} 
					setPage={this.set_page.bind(this)} />
			</div>
		)
	}
}