import React from 'react';
import Moment from 'moment';

export default class SearchData extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      isShow: false,
      triangleIcon: 'glyphicon-triangle-top'
    };
  }
  componentDidMount() {
    document.getElementById('SearchScope').style.display = "none";

    $('#searchSDate, #searchEDate').datepicker({
      format: 'yyyy-mm-dd',
      orientation: "bottom auto",
      autoclose: true
    });
  }
  _callSearch() {
    this.setState({
      isShow: !this.state.isShow,
      triangleIcon: (!this.state.isShow) ? 'glyphicon-triangle-bottom' : 'glyphicon-triangle-top'
    }, () => {
      document.getElementById('SearchScope').style.display = (this.state.isShow) ? 'block' : 'none';
    });
  }
  _goDatePage() {
    let url = window.location.pathname,
        params = get_url_vars(),
        sDate = $('#searchSDate').val(),
        eDate = $('#searchEDate').val();

    if(Moment(sDate + ' 00:00:00').valueOf() < Moment(eDate + ' 00:00:00').valueOf()) {
      let query = 'sDate=' + $('#searchSDate').val() + '&' + 'eDate=' + $('#searchEDate').val();

      window.history.pushState(null, null, url + '?' + query);

      this._callSearch();

      $('#searchSDate').val('');
      $('#searchEDate').val('');

      this.props.setSearchDate(sDate, eDate, '1');
    } else {
      alert('開始時間不得小於結束時間');
      return;
    }
  }
  render() {
    return (
      <div className="">
        <a href="javascript:;" onClick={this._callSearch.bind(this)}>
          搜尋日期
          <span className={['glyphicon', this.state.triangleIcon].join(' ')}></span>
        </a>
        <hr />
        <div id="SearchScope">
          <div className="form-inline">
            <div className="form-group SearchSDateLabel">
              <label htmlFor="searchSDate">開始時間</label>
            </div>
            <div className="form-group input-group-sm">
              <input type="text" className="form-control" id="searchSDate" />
            </div>
            <div className="form-group SearchEDateLabel">
              <label htmlFor="searchEDate">結束時間</label>
            </div>
            <div className="form-group input-group-sm">
              <input type="text" className="form-control" id="searchEDate" />
            </div>
            <div className="form-group SearchBtn">
              <button type="button" className="btn btn-primary btn-sm" onClick={this._goDatePage.bind(this)}>搜尋</button>
            </div>
          </div>
          <hr />
        </div>
      </div>
    );
  }
}