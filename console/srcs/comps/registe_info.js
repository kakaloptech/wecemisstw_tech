import React from 'react';

const UploadImage = (props) => {
	return (
		<div className="col-md-4">
	  	<div className="panel panel-default">
			  <div className="panel-body">
			    <div className="thumbnail" id="ImageFile">
			    	<img src="images/prepare.jpg" />
			    </div>
			    <div className="form-group">
		      	<input className="form-control" type="file" onChange={(event) => {
		      		props.setFileIndex(props.types, props.index); 
		      		props.getFile(event);
		      	}} />
		      </div>
			  </div>
			</div>
	  </div>
	)
}

export default class RegisteInfo extends React.Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	imageIndex: 0,
	  	passportImgs: [1, 2],
	  	selfImgs: [1, 2, 3],
	  	registeInfo: {
	  		nameCN: '',
	  		nameEN: '',
	  		citizenship: '',
	  		birthday: '',
	  		birthplace: '',
	  		age: '',
	  		reside: '',
	  		identity: '',
	  		postal_code: '',
	  		address: '',
	  		tel: '',
	  		phone: '',
	  		email: '',
	  		education: '',
	  		profession: '',
	  		occupation: '',
	  		language: '',
	  		jobs: '',
	  		height: '',
			  weight: '',
			  chest: '',
			  waist: '',
			  buttocks: '',
			  interest: '',
			  introduce: '',
			  fb: '',
			  youtube: '',
			  selfImages: ['', ''],
			  passportImages: ['', '', '', '', '', ''],
			  guardianName: '',
			  guardianRelatio: '',
			  guardianPostal: '',
			  guardianAddress: '',
			  guardianPhone: '',
			  guardianTel: ''
	  	}
	  };
	}
	componentDidMount() {
		$('#birthday').datepicker({
      format: 'yyyy-mm-dd',
      orientation: "bottom auto",
      autoclose: true
    });

    let id = get_url_vars()['id'];
    if(id) {
    	const cbResult = (result) => {
				this.setState({
					registeInfo: result
				});
			}

			post_data('procs.php', {type: 'getRegisteInfo', id: id}, cbResult);
    }
	}
	set_file_index(type, imageIndex) {
		if(type === 'self') imageIndex += 2;
		this.setState({
			imageIndex
		});
	}
	get_file(event) {
		let file = event.target.files[0],
				fReader = new FileReader();  

    fReader.onload = (subEvent) => {
      let temp = '<img src="' + subEvent.target.result + '" />';
      $('div#ImageFile').eq(this.state.imageIndex).html(temp);

      const cbResult = (result) => {
				let imageIndex = this.state.imageIndex,
						registeInfo = this.state.registeInfo,
						num = (imageIndex > 2) ? Math.ceil(imageIndex - 2) : imageIndex;

				if(imageIndex < 2) {
					registeInfo['selfImages'][num] = result['imagePath'];
				} else {
					registeInfo['passportImages'][num] = result['imagePath'];
				}
				this.setState({
					registeInfo
				});
			}

			post_data('procs.php', {
				type: 'CreateImage',
				data: subEvent.target.result
			}, cbResult);
    };

    fReader.readAsDataURL(file);
	}
	set_registe_info(event) {
		let registeInfo = this.state.registeInfo,
				key = event.target.dataset.name,
				value = event.target.value;

		registeInfo[key] = value;

		this.setState({
			registeInfo
		});
	}
	send_data() {
		let data = this.state.registeInfo,
				id = get_url_vars()['id'];

		data['type'] = 'UpdateRegiste';
		data['id'] = id;

		const cbResult = (result) => {
			alert(result['message'] || '更新成功');
			return;
		}

		post_data('procs.php', data, cbResult);
	}

	render() {
		return (
			<div>
				<div className="form-group">
			    <label className="col-md-2 control-label">中文姓名</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.nameCN} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="nameCN" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">英文姓名</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.nameEN} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="nameEN" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">國籍</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.citizenship} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="citizenship" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">生日</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.birthday} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="birthday"
			      				id="birthday" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">出生地</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.birthplace} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="birthplace" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">年齡</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.age} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="age" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">現居國家及城市</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.reside} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="reside" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">身分證</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.identity} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="identity" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">室內電話</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.tel} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="tel" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">手機號碼</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.phone} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="phone" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">電子信箱</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.email} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="email" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">地址</label>
			    <div className="col-md-2 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.postal_code} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="postal_code" />
			    </div>
			    <div className="col-md-8 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.address} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="address" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">學歷</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.education} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="education" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">專業</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.profession} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="profession" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">職業</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.occupation} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="occupation" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">語言能力 / 語種</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.language} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="language" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">現在所在工作單位/學校</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.jobs} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="jobs" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">身高</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.height} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="height" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">體重</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.weight} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="weight" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">胸圍</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.chest} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="chest" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">腰圍</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.waist} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="waist" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">臀圍</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.buttocks} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="buttocks" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">興趣/愛好/特長</label>
			    <div className="col-md-10 RegisteColumn">
			      <textarea className="form-control" style={{height: 150}}
			      					value={this.state.registeInfo.interest} 
				      				onChange={this.set_registe_info.bind(this)} 
				      				data-name="interest" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">自我介紹</label>
			    <div className="col-md-10 RegisteColumn">
			      <textarea className="form-control" style={{height: 150}}
			      					value={this.state.registeInfo.introduce} 
				      				onChange={this.set_registe_info.bind(this)} 
				      				data-name="introduce" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">FB粉絲團</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.fb} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="fb" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">youtube頻道</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.youtube} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="youtube" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">證件照</label>
			    <div className="col-md-10 RegisteColumn">
			      <div className="row">
							{
								this.state.passportImgs.map((value, key) => {
									return (
										<UploadImage 
											setFileIndex={this.set_file_index.bind(this)}
											getFile={this.get_file.bind(this)}
											types={"passport"}
											index={key}
											key={key} />
									)
								})
							}						  
						</div>
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">自我介紹圖片</label>
			    <div className="col-md-10 RegisteColumn">
			      <div className="row">
						  {
								this.state.selfImgs.map((value, key) => {
									return (
										<UploadImage 
											setFileIndex={this.set_file_index.bind(this)}
											getFile={this.get_file.bind(this)}
											types={"self"}
											index={key}
											key={key} />
									)
								})
							}
					  </div>
					  <div className="row">
						  {
								this.state.selfImgs.map((value, key) => {
									return (
										<UploadImage 
											setFileIndex={this.set_file_index.bind(this)}
											getFile={this.get_file.bind(this)}
											types={"self"}
											index={Math.ceil(key + 3)}
											key={key} />
									)
								})
							}
						</div>
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">監護人姓名</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.guardianName} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="guardianName" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">監護人與參賽者關係</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.guardianRelatio} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="guardianRelatio" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">監護人地址</label>
			    <div className="col-md-2 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.guardianPostal} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="guardianPostal" />
			    </div>
			    <div className="col-md-8 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.guardianAddress} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="guardianAddress" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">監護人電話</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.guardianTel} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="guardianTel" />
			    </div>
			  </div>
			  <div className="form-group">
			    <label className="col-md-2 control-label">監護人行動電話</label>
			    <div className="col-md-10 RegisteColumn">
			      <input className="form-control" type="text"
			      				value={this.state.registeInfo.guardianPhone} 
			      				onChange={this.set_registe_info.bind(this)} 
			      				data-name="guardianPhone" />
			    </div>
			  </div>
			  <div className="form-group">
			    <a href="javascript:;" className="btn btn-primary"
			    		onClick={this.send_data.bind(this)}>修改</a>
			    <a href="javascript:;" className="btn btn-default"
			    		onClick={() => {
			    			window.location.href = 'registe.php'
			    		}}>取消</a>
			  </div>
			</div>
		)
	}
}