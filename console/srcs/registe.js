import React from 'react';
import ReactDOM from 'react-dom';

import { TopMenu } from './topmenu';
import RegisteList from './comps/registe_list';


const Registe = () => {
	return (
		<div>
			<TopMenu title={camName} />
			<div className="MainScope">
				<ol className="breadcrumb">
				  <li className="active">報名列表</li>
				</ol>
				<RegisteList />
			</div>
		</div>
	)
}

ReactDOM.render(
	<Registe />,
	document.getElementById('MainBox')
);

$('.TopMenu li').eq(0).addClass('active');