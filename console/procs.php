<?php

	include('./init/b_init.php');
	if($_SERVER['HTTP_X_CSRF_TOKEN'] !== $Tokens->get_token()) {
		echo json_encode(array(
			"status" => false,
			"message" => "操作錯誤, error: -100"
		));
		die();
	}

	$temp = $_POST;
	$actionType = $temp['type'];
	unset($temp['type']);

	if(isset($temp) && !empty($temp)) {
		foreach ($temp as $key => $value) {
			if($key !== 'selfImages' && $key !== 'passportImages') {
				$temp[$key] = $XssClean->clean_input($value);
			}
		}
	}

	switch ($actionType) {
		case 'getRegiste':
			$data = $Actions->generate_seach($temp);
			$Actions->get_data('/get_registe_data', $data);
			break;
		case 'getTransfer':
			$data = $Actions->generate_seach($temp);
			$Actions->get_data('/get_payment_data', $data);
			break;
		case 'getRegisteInfo':
			$Actions->get_data('/get_registe_by_id', $temp);
			break;
		case 'CreateImage':
			$Actions->post_data('/create/image', $temp);
			break;
		case 'UpdateRegiste':
			$Actions->post_data('/update_registe_data', $temp);
			break;
		default:
			echo json_encode(array(
				"status" => false,
				"message" => "操作錯誤"
			));
			break;
	}

?>