<?php 
	include('./init/f_init.php'); // 宣告前端常數

	$Tokens->set_token(); // 建立token
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo CAMNAME; ?> 登入頁面</title>
	<?php include('header.php'); ?>
	<style style="text/css">
    .bs-example {
      position: relative;
      background-color: #fff;
      border-style:solid;
      border-color: #ddd;
      border-radius: 4px 4px 0 0;
      border-width: 1px;
      box-shadow: none;
      margin-top: 50px;
      padding: 10px;
    }

    .LoginBtnLeft {
      margin: auto 10px;
    }
  </style>
</head>
<body>
	<div class="container" id="MainBox"></div>
	<script type="text/javascript" defer>
    var _token = "<?php echo $Tokens->get_token(); ?>";
    var camName = "<?php echo CAMNAME; ?>";
  </script>
  <script type="text/javascript" defer src="/js/allfns.js"></script>
  <script type="text/javascript" defer src="//localhost:8080/comps/common.min.js"></script>
  <script type="text/javascript" defer src="//localhost:8080/comps/login.min.js"></script>
</body>
</html>