var post_data = function(url, data, cb) {
  $.ajaxSetup({
    headers: {
      "X-CSRF-Token": _token
    }
  });

  $.post(url, data, cb, 'json');
}

var get_data = function(url, data, cb) {
  $.ajaxSetup({
    headers: {
      "X-CSRF-Token": _token
    }
  });

  $.get(url, data, cb, 'json');
}

var get_url_vars = function() {
  var vars = [], hash;
  var hashes = decodeURIComponent(window.location.href.slice(window.location.href.indexOf('?') + 1)).split('&');
  for(var i = 0; i < hashes.length; i++){
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}